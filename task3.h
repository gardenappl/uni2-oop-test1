#pragma once

#include <memory>
#include <vector>

namespace task3
{
	extern int _alpha_n;
	extern int _beta_n;
	extern int _gamma_n;
	extern int _delta_n;
	extern int _s;
	
	struct base1;
	struct base2;
	struct base
	{
		std::shared_ptr<base1> child1 = nullptr;
		std::shared_ptr<base2> child2_1 = nullptr;
		std::shared_ptr<base2> child2_2 = nullptr;

		int n;
		virtual void on_destruction(int& s) {};
		base(int n);
		virtual ~base() {}
	};

	struct base1 : public base
	{
		base1(int n);
		void on_destruction(int& s);
		virtual ~base1();
	};

	struct alpha : public base1
	{
		alpha();
		void on_destruction(int& s);
		~alpha();
	};

	struct beta : public base1
	{
		beta();
		void on_destruction(int& s);
		~beta();
	};

	struct base2 : public base
	{
		base2(int n);
		void on_destruction(int& s);
		virtual ~base2();
	};

	struct gamma : public base2
	{
		gamma();
		void on_destruction(int& s);
		~gamma();
	};

	struct delta : public base2
	{
		delta();
		void on_destruction(int& s);
		~delta();
	};

	int simulate_deletion(std::vector<std::shared_ptr<base>>& objects);
}