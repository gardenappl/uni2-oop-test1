#include "laws.h"
#include "democratic_world.h"

namespace demo
{
	law::~law() {}

	law::law(float complexity)
		: complexity(complexity)
	{}

	bool law::can_be_removed()
	{
		return true;
	}

	generic_law::generic_law(float complexity, std::string description)
		: law(complexity), description(description)
	{}

	void generic_law::on_passed(world* the_world, parliament* the_parliament)
	{
		//no-op
	}

	bool generic_law::is_compatible(law* other_law)
	{
		return true;
	}

	std::string generic_law::get_description()
	{
		return description;
	}



	std::string random_law_selection_law::get_description()
	{
		return "Each party selects a law to discuss; The one discussed law must then be selected randomly.";
	}

	void random_law_selection_law::on_passed(world* the_world, parliament* the_parliament)
	{
		the_parliament->law_selection_process =
			the_world->law_selection_process_components[law_selection_component_ids::random].get();
	}

	void majority_law_selection_law::on_passed(world* the_world, parliament* the_parliament)
	{
		the_parliament->law_selection_process =
			the_world->law_selection_process_components[law_selection_component_ids::majority].get();
	}
}