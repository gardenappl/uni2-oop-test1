#pragma once

#include <memory>
#include <unordered_map>
#include "democratic_world.fwd.h"

namespace demo
{
	class law_selection_process_component
	{
	public:
		virtual size_t select_next_law(std::unordered_map<party*, size_t>* preferred_laws, parliament* parliament, world* the_world) const = 0;
		virtual ~law_selection_process_component() {}
	};

	class election_process_component
	{
	public:
		virtual void do_election(parliament* parliament, voting_results* results) const = 0;
		virtual ~election_process_component() {}
	};

	class law_debate_length_component
	{
	public:
		virtual int get_debate_length(parliament* parliament, law* current_law) const = 0;
		virtual ~law_debate_length_component() {}
	};

	class law_vote_threshold_component
	{
	public:
		virtual float get_vote_threshhold(parliament* parliament, law* current_law) const = 0;
		virtual ~law_vote_threshold_component() {};
	};

	//Each party can have multiple strategies, used consecutively
	//Each "strategy" performs a stable sort of the laws vector by some criteria
	//In the end, laws[0] is the law that the party decides to vote for.

	class party_law_selection_strategy
	{
	public:
		virtual void select_preferred_law(std::vector<size_t>& laws, party* the_party, world* the_world, parliament* the_parliament) = 0;
		virtual ~party_law_selection_strategy() {};
	};

	class party_vote_strategy
	{
	public:
		//How likely is it that a representative supports a particular law
		virtual float calculate_approval_likelihood(law* a_law, parliament* the_parliament, world* the_world) = 0;
		virtual ~party_vote_strategy() {};
	};

	class voter_election_strategy
	{
	public:
		virtual void select_preferred_law(std::vector<party*> parties, voter* the_voter, world* the_world) = 0;
		virtual ~voter_election_strategy() {};
	};

	//Subtypes of components

	enum law_selection_component_ids : size_t
	{
		random = 0,
		majority = 1
	};

	class majority_law_selection : public law_selection_process_component
	{
	public:
		size_t select_next_law(std::unordered_map<party*, size_t>* preferred_laws,
			parliament* the_parliament, world* the_world) const;
	};

	class random_law_selection : public law_selection_process_component
	{
	public:
		size_t select_next_law(std::unordered_map<party*, size_t>* preferred_laws,
			parliament* the_parliament, world* the_world) const;
	};

	//Final subtask of task 1

	class voter_popularity_party_vote_strategy : public party_law_selection_strategy
	{
	private:
		std::unordered_map<size_t, int> law_popularities;

	public:
		void init_cache(std::vector<size_t>& laws, party* the_party, world* the_world, parliament* the_parliament);
		bool compare(size_t law1, size_t law2);
		void select_preferred_law(std::vector<size_t>& laws, party* the_party, world* the_world, parliament* the_parliament);
	};
	class party_popularity_party_vote_strategy : public party_law_selection_strategy
	{
	private:
		std::unordered_map<size_t, int> law_popularities;

	public:
		void init_cache(std::vector<size_t>& laws, party* the_party, world* the_world, parliament* the_parliament);
		bool compare(size_t law1, size_t law2);
		void select_preferred_law(std::vector<size_t>& laws, party* the_party, world* the_world, parliament* the_parliament);
	};
	class voter_hatred_party_vote_strategy : public party_law_selection_strategy
	{
	private:
		std::unordered_map<size_t, int> law_hatred;

	public:
		void init_cache(std::vector<size_t>& laws, party* the_party, world* the_world, parliament* the_parliament);
		bool compare(size_t law1, size_t law2);
		void select_preferred_law(std::vector<size_t>& laws, party* the_party, world* the_world, parliament* the_parliament);
	};
	class party_hatred_party_vote_strategy : public party_law_selection_strategy
	{
	private:
		std::unordered_map<size_t, int> law_hatred;

	public:
		void init_cache(std::vector<size_t>& laws, party* the_party, world* the_world, parliament* the_parliament);
		bool compare(size_t law1, size_t law2);
		void select_preferred_law(std::vector<size_t>& laws, party* the_party, world* the_world, parliament* the_parliament);
	};

	class complex_party_vote_strategy : public party_law_selection_strategy
	{
	private:
		float party_popularity_weight;
		float party_hatred_weight;
		float voter_popularity_weight;
		float voter_hatred_weight;

	public:
		complex_party_vote_strategy(float party_popularity_weight, float party_hatred_weight,
				float voter_popularity_weight, float voter_hatred_weight);
		void select_preferred_law(std::vector<size_t>& laws, party* the_party, world* the_world, parliament* the_parliament);
	};

}