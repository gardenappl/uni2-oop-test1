#include <algorithm>
#include <unordered_map>
#include <random>
#include <stdexcept>
#include "components.h"
#include "democratic_world.h"

namespace demo
{
	//Exampe of subtypes of components
	size_t majority_law_selection::select_next_law(std::unordered_map<party*, size_t>* preferred_laws,
		parliament* the_parliament, world* the_world) const
	{
		std::unordered_map<size_t, int> law_votes;
		for (auto iter = preferred_laws->cbegin(); iter != preferred_laws->cend(); iter++)
		{
			auto party_id = std::find(the_parliament->parties.begin(), the_parliament->parties.end(), iter->first);
			law_votes[iter->second] += the_parliament->representatives_count[party_id];
		}
		return std::max_element(law_votes.cbegin(), law_votes.cend(),
				[](const std::pair<size_t, int>& p1, const std::pair<size_t, int>& p2)
				{ return p1.second < p2.second; })->first;
	}

	size_t random_law_selection::select_next_law(std::unordered_map<party*, size_t>* preferred_laws,
			parliament* the_parliament, world* the_world) const
	{
		std::uniform_int_distribution<size_t> dist(0, preferred_laws->size() - 1);
		size_t chosen_index = dist(the_world->rng);
		
		auto iter = preferred_laws->cbegin();
		for(int i = 0; i < chosen_index; i++)
		{
			if (iter == preferred_laws->cend())
				throw std::logic_error("Random index is out of bounds");
			iter++;
		}
		return iter->second;
	}


	void voter_popularity_party_vote_strategy::init_cache
			(std::vector<size_t>& laws, party* the_party, world* the_world, parliament* the_parliament)
	{
		law_popularities.clear();
		for (size_t law_id : laws)
		{
			law_popularities[law_id] = 0;
			for (auto& voter : the_world->voters)
			{
				if (std::find(voter->supported_laws.cbegin(), voter->supported_laws.cend(), law_id)
					!= voter->supported_laws.cend())
				{
					law_popularities[law_id]++;
				}
			}
		}
	}

	bool voter_popularity_party_vote_strategy::compare(size_t law_id1, size_t law_id2)
	{
		return this->law_popularities[law_id1] < this->law_popularities[law_id2];
	}

	void voter_popularity_party_vote_strategy::select_preferred_law
			(std::vector<size_t>& laws, party* the_party, world* the_world, parliament* the_parliament)
	{
		init_cache(laws, the_party, the_world, the_parliament);
		std::stable_sort(laws.begin(), laws.end(), compare);
	}



	void voter_hatred_party_vote_strategy::init_cache
	(std::vector<size_t>& laws, party* the_party, world* the_world, parliament* the_parliament)
	{
		law_hatred.clear();
		for (size_t law_id : laws)
		{
			law_hatred[law_id] = 0;
			for (auto& voter : the_world->voters)
			{
				if (std::find(voter->opposed_laws.cbegin(), voter->opposed_laws.cend(), law_id)
					!= voter->opposed_laws.cend())
				{
					law_hatred[law_id]++;
				}
			}
		}
	}

	bool voter_hatred_party_vote_strategy::compare(size_t law_id1, size_t law_id2)
	{
		return this->law_hatred[law_id1] < this->law_hatred[law_id2];
	}

	void voter_hatred_party_vote_strategy::select_preferred_law
	(std::vector<size_t>& laws, party* the_party, world* the_world, parliament* the_parliament)
	{
		init_cache(laws, the_party, the_world, the_parliament);
		std::stable_sort(laws.begin(), laws.end(), compare);
	}



	void party_popularity_party_vote_strategy::init_cache
	(std::vector<size_t>& laws, party* the_party, world* the_world, parliament* the_parliament)
	{
		law_popularities.clear();
		for (size_t law_id : laws)
		{
			law_popularities[law_id] = 0;
			for (auto& party : the_world->parties)
			{
				if (std::find(party->supported_laws.cbegin(), party->supported_laws.cend(), law_id)
					!= party->supported_laws.cend())
				{
					law_popularities[law_id]++;
				}
			}
		}
	}

	bool party_popularity_party_vote_strategy::compare(size_t law_id1, size_t law_id2)
	{
		return this->law_popularities[law_id1] < this->law_popularities[law_id2];
	}

	void party_popularity_party_vote_strategy::select_preferred_law
	(std::vector<size_t>& laws, party* the_party, world* the_world, parliament* the_parliament)
	{
		init_cache(laws, the_party, the_world, the_parliament);
		std::stable_sort(laws.begin(), laws.end(), compare);
	}



	void party_hatred_party_vote_strategy::init_cache
	(std::vector<size_t>& laws, party* the_party, world* the_world, parliament* the_parliament)
	{
		law_hatred.clear();
		for (size_t law_id : laws)
		{
			law_hatred[law_id] = 0;
			for (auto& party : the_world->parties)
			{
				if (std::find(party->supported_laws.cbegin(), party->supported_laws.cend(), law_id)
					!= party->supported_laws.cend())
				{
					law_hatred[law_id]++;
				}
			}
		}
	}

	bool party_hatred_party_vote_strategy::compare(size_t law_id1, size_t law_id2)
	{
		return this->law_hatred[law_id1] < this->law_hatred[law_id2];
	}

	void party_hatred_party_vote_strategy::select_preferred_law
		(std::vector<size_t>& laws, party* the_party, world* the_world, parliament* the_parliament)
	{
		init_cache(laws, the_party, the_world, the_parliament);
		std::stable_sort(laws.begin(), laws.end(), compare);
	}

	complex_party_vote_strategy::complex_party_vote_strategy
		(float party_popularity_weight, float party_hatred_weight,
		float voter_popularity_weight, float voter_hatred_weight)
		: party_popularity_weight(party_popularity_weight),
		party_hatred_weight(party_hatred_weight),
		voter_popularity_weight(voter_popularity_weight),
		voter_hatred_weight(voter_hatred_weight)
	{}

	void complex_party_vote_strategy::select_preferred_law
		(std::vector<size_t>& laws, party* the_party, world* the_world, parliament* the_parliament)
	{
		party_popularity_party_vote_strategy party_popularity;
		voter_popularity_party_vote_strategy voter_popularity;
		party_hatred_party_vote_strategy party_hatred;
		voter_hatred_party_vote_strategy voter_hatred;

		party_popularity.init_cache(laws, the_party, the_world, the_parliament);
		voter_popularity.init_cache(laws, the_party, the_world, the_parliament);
		party_hatred.init_cache(laws, the_party, the_world, the_parliament);
		voter_hatred.init_cache(laws, the_party, the_world, the_parliament);

		std::stable_sort(laws.begin(), laws.end(),
			[&](size_t law_id1, size_t law_id2)
			{
				return party_popularity_weight * party_popularity.compare(law_id1, law_id2) +
					voter_popularity_weight * voter_popularity.compare(law_id1, law_id2) +
					party_hatred_weight * party_hatred.compare(law_id1, law_id2) +
					voter_hatred_weight * voter_hatred.compare(law_id1, law_id2);
			});
	}
}