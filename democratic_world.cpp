#include <memory>
#include <unordered_map>
#include <algorithm>
#include <numeric>
#include <vector>
#include <iostream>
#include "democratic_world.h"
#include "laws.h"
#include "components.h"

namespace demo
{
	party::party(std::vector<std::unique_ptr<party_law_selection_strategy>>&& law_selection,
		party_vote_strategy* vote_strategy)
		: election_strategies(std::move(law_selection)),
		approval_likelihood_strategy(std::unique_ptr<party_vote_strategy>(vote_strategy))
	{}

	voter::voter(std::vector<std::unique_ptr<voter_election_strategy>>&& law_selection)
		: election_strategies(std::move(law_selection))
	{}

	size_t party::select_preferred_law(std::vector<size_t>& available_laws, world* the_world, parliament* the_parliament)
	{
		std::vector<size_t> considered_laws(available_laws);
		for (auto& strategy : election_strategies)
			strategy->select_preferred_law(considered_laws, this, the_world, the_parliament);
		if (considered_laws.empty())
			return available_laws[0]; //the party doesn't care, pick whatever
		else
			return considered_laws[0]; //the party thinks this law is the most worthy of discussion
	}

	float party::calculate_approval(law* a_law, parliament* the_parliament, world* the_world)
	{
		return approval_likelihood_strategy->calculate_approval_likelihood(a_law, the_parliament, the_world);
	}

	party* voter::select_preferred_law(std::vector<party*>& parties, world* the_world)
	{
		std::vector<party*> considered_parties(parties);
		for (auto& strategy : election_strategies)
			strategy->select_preferred_law(considered_parties, this, the_world);
		return considered_parties[0];
	}


	parliament::parliament(world* the_world, int term_length,
			law_selection_process_component* law_selection_process,
			election_process_component* election_process,
			law_debate_length_component* law_debate_length,
			law_vote_threshold_component* law_vote_threshold)
		: the_world(the_world), term_length(term_length),
		law_selection_process(law_selection_process), election_process(election_process),
		law_debate_length(law_debate_length), law_vote_threshold(law_vote_threshold)
	{}

	std::unique_ptr<std::unordered_map<party*, size_t>> parliament::select_preferred_law()
	{
		auto selected_laws = new std::unordered_map<party*, size_t>();
		std::vector<size_t> all_law_ids;
		for (size_t i = 0; i < the_world->all_laws.size; i++)
			all_law_ids.push_back(i);

		for (party* a_party : parties)
			(*selected_laws)[a_party] = a_party->select_preferred_law(all_law_ids, the_world, this);

		return std::unique_ptr<std::unordered_map<party*, size_t>>(selected_laws);
	}

	void parliament::debate_law()
	{
		current_law_debate_length++;

		law* current_debated_law = the_world->all_laws[current_debated_law_id];
		if (current_law_debate_length >= law_debate_length->get_debate_length(this, current_debated_law))
		{
			int total_representatives = 0;
			int approvals = 0;
			for (auto iterator = representatives_count.cbegin(); iterator != representatives_count.cend(); iterator++)
			{
				total_representatives += iterator->second;
				approvals += parties[iterator->first]->calculate_approval(current_debated_law, this, the_world);
			}

			float vote_result = (float)approvals / total_representatives;
			if (vote_result > law_vote_threshold->get_vote_threshhold(this, current_debated_law))
			{
				std::cout << "The law is approved!\n";
				if (std::find(the_world->current_laws.cbegin(), the_world->current_laws.cend(), current_debated_law)
					== the_world->current_laws.cend())
				{
					for (auto iter = the_world->current_laws.begin(); iter != the_world->current_laws.end();)
					{
						if (!(*iter)->is_compatible(current_debated_law))
							the_world->current_laws.erase(iter);
						else
							iter++;
					}
					std::cout << "This law has been passed.\n";

					the_world->current_laws.push_back(current_debated_law);
					current_debated_law->on_passed(the_world, this);
				}
			}
			else
			{
				std::cout << "The law is not approved!\n";
				if (current_debated_law->can_be_removed())
				{
					auto position = std::find(the_world->current_laws.cbegin(), the_world->current_laws.cend(), current_debated_law);
					if (position != the_world->current_laws.cend())
					{
						std::cout << "This law no longer applies.\n";
						the_world->current_laws.erase(position);
					}
				}
			}
		}
	}

	bool parliament::pass_day()
	{
		bool will_debate_today = true;
		if (!current_debated_law_id.has_value())
		{
			auto preferred_laws = select_preferred_law();
			current_debated_law_id =
				law_selection_process->select_next_law(preferred_laws.get(), this, this->the_world);

			//nothing to discuss today
			if (!current_debated_law_id.has_value())
				will_debate_today = false;
		}

		if(will_debate_today)
			debate_law();

		if (the_world->current_day - last_election_day >= term_length)
			the_world->do_election();

		return true;
	}

	void world::do_election()
	{
		auto election_results = std::make_unique<voting_results>();
		std::vector<party*> available_parties;
		std::transform(parties.cbegin(), parties.cend(), available_parties.begin(),
			[](std::unique_ptr<party>& party_ptr) -> party* { return party_ptr.get(); });
		for (auto& voter : voters)
		{
			election_results->vote_count[voter->select_preferred_law(available_parties, this)]++;
		}
		the_parliament.election_process->do_election(&the_parliament, election_results.get());
	}

	bool world::pass_day()
	{
		current_day++;

		the_parliament.pass_day();
	}
}