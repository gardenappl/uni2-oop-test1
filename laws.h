#pragma once

#include <string>
#include "democratic_world.fwd.h"

namespace demo
{
	class law
	{
		float complexity;

	public:
		law(float complexity);

		virtual std::string get_description() = 0;
		virtual void on_passed(world* the_world, parliament* the_parliament) = 0;
		//Some laws are non-removable, e. g. you can't change remove a law
		//that describes the election process,
		//You must replace it with a new one!
		virtual bool can_be_removed();
		virtual bool is_compatible(law* other_law) = 0;
	public:
		virtual ~law();
	};

	class generic_law : public law
	{
	private:
		std::string description;

	public:
		generic_law(float complexity, std::string description);
		void on_passed(world* the_world, parliament* the_parliament);
		bool is_compatible(law* other_law);
		std::string get_description();
	};

	class law_selection_law : public law
	{
		bool is_compatible(law* other_law);
	};

	class random_law_selection_law : public law
	{
		std::string get_description();
		void on_passed(world* the_world, parliament* the_parliament);
	};

	class majority_law_selection_law : public law
	{
		std::string get_description();
		void on_passed(world* the_world, parliament* the_parliament);
	};
}

