#include "task3.h"
#include <iostream>
#include <map>

namespace task3
{
	int _alpha_n = 1;
	int _beta_n = 1;
	int _gamma_n = 1;
	int _delta_n = 1;
	int _s = 0;

	base::base(int n)
		: n(n)
	{}

	base1::base1(int n)
		: base(n)
	{
		std::cout << "base1 " << n << " constructed\n";
	}

	void base1::on_destruction(int& s)
	{
		s = 3 * s + n + 27;
		std::cout << "new s: " << s << '\n';
		std::cout << "base1 " << n << " destructed\n";
	}

	base1::~base1()
	{
		on_destruction(task3::_s);
	}


	base2::base2(int n)
		: base(n)
	{
		std::cout << "base2 " << n << " constructed\n";
	}

	void base2::on_destruction(int& s)
	{
		s = s / 2 - n;
		std::cout << "new s: " << s << '\n';
		std::cout << "base2 " << n << " destructed\n";
	}

	base2::~base2()
	{
		on_destruction(task3::_s);
	}


	alpha::alpha() : base1(_alpha_n++)
	{
		std::cout << "alpha " << n << " constructed\n";
	}

	void alpha::on_destruction(int& s)
	{
		s = s - 2 * n + 5;
		std::cout << "new s: " << s << '\n';
		std::cout << "alpha " << n << " destructed\n";
	}

	alpha::~alpha()
	{
		on_destruction(task3::_s);
	}


	beta::beta() : base1(_beta_n++)
	{
		std::cout << "beta " << n << " constructed\n";
	}

	void beta::on_destruction(int& s)
	{
		s = s - 2 * n + 5;
		std::cout << "new s: " << s << '\n';
		std::cout << "beta " << n << " destructed\n";
	}

	beta::~beta()
	{
		on_destruction(task3::_s);
	}

	gamma::gamma() : base2(_gamma_n++)
	{
		std::cout << "gamma " << n << " constructed\n";
	}

	void gamma::on_destruction(int& s)
	{
		s = s - n;
		std::cout << "new s: " << s << '\n';
		std::cout << "gamma " << n << " destructed\n";
	}

	gamma::~gamma()
	{
		on_destruction(task3::_s);
	}


	delta::delta() : base2(_delta_n++)
	{
		std::cout << "delta " << n << " constructed\n";
	}

	void delta::on_destruction(int& s)
	{
		s = s + 3 * n - 27;
		std::cout << "new s: " << s << '\n';
		std::cout << "delta " << n << " destructed\n";
	}

	delta::~delta()
	{
		on_destruction(task3::_s);
	}

	//might get stuck on cyclic dependencies, let's pretend those don't exist :)
	void count_refs(std::map<base*, int>& ref_count, base* current_obj)
	{
		ref_count[current_obj]++;
		if (current_obj->child1)
			count_refs(ref_count, current_obj->child1.get());
		if (current_obj->child2_1)
			count_refs(ref_count, current_obj->child2_1.get());
		if (current_obj->child2_2)
			count_refs(ref_count, current_obj->child2_2.get());
	}

	void remove_ref(std::map<base*, int>& ref_count, base* current_obj, int& s)
	{
		ref_count[current_obj]--;
		//call subclass destructor, base destructor, then field destructors
		if (ref_count[current_obj] == 0)
		{
			current_obj->on_destruction(s);
			if (base1* base1_ptr = dynamic_cast<base1*>(current_obj))
				base1_ptr->base1::on_destruction(s);
			if (base2* base2_ptr = dynamic_cast<base2*>(current_obj))
				base2_ptr->base2::on_destruction(s);
		}
		if(current_obj->child2_2)
			remove_ref(ref_count, current_obj->child2_2.get(), s);
		if (current_obj->child2_1)
			remove_ref(ref_count, current_obj->child2_1.get(), s);
		if (current_obj->child1)
			remove_ref(ref_count, current_obj->child1.get(), s);
	}

	int simulate_deletion(std::vector<std::shared_ptr<base>>& objects)
	{
		int simulated_s = 0;

		std::map<base*, int> ref_count;
		for (size_t i = 0; i < objects.size(); i++)
		{
			count_refs(ref_count, objects[i].get());
		}

		/*for (auto elem : ref_count)
		{
			std::cout << typeid(elem.first).name() << " " << elem.first->n << " " << elem.second << "\n";
		}*/

		for (int i = objects.size() - 1; i >= 0; i--)
		{
			remove_ref(ref_count, objects[i].get(), simulated_s);
		}

		return simulated_s;
	}
}