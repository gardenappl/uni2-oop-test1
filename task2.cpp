#include "task2.h"

#include <cctype>
#include <vector>

template<>
int task2::f(char const* x)
{
	std::cout << "x is a string.\n";
	int result = 0;
	int i = 0;
	while(true)
	{
		char c = x[i];
		if (c == 0)
			break;
		if (c >= 'A' && c <= 'Z') //TO-DO: consider something more portable?
			result++;
		i++;
	}
	return result;
}

template<>
int task2::f(int x)
{
	if (x > 0)
		return f_positive_int(x);
	else if (x < 0)
		return f_negative_int(x);
	else
		return default_value;
}

template<>
int task2::f(float x)
{
	std::cout << "x is a floating point type.\n";
	return (int)(1 / sin(77 * x)) % 369;
}