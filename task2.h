#pragma once

#include <type_traits>
#include <cmath>
#include <iostream>
#include <typeinfo>
#include <string>
#include <vector>
#include <utility>

class task2
{
private:
	template<typename T>
	static int factorial_mod(T n, int p);
	template<typename T>
	static int f_positive_int(T x);
	template<typename T>
	static int f_negative_int(T x);
	static const int default_value = 7487;

public:
    //This would enable templates for all numeric types
    //(my current implementation only accepts int and float)
    //But this also over-complicates the rest of the task
    
	/*template<typename T>
	static std::enable_if_t<std::is_integral_v<T>, int> f(T x)
	{
		if (x > 0)
			return f_positive_int(x);
		else if (x < 0)
			return f_negative_int(x);
		else
			return default_value;
	}*/

	/*template<typename T>
	static std::enable_if_t<std::is_floating_point_v<T>, int> f(T x)
	{
		std::cout << "x is a floating point type.\n";
		return (int)(1 / sin(77 * x)) % 369;
	}*/

	
    //Another alternative to consider might be C++20 concepts/constraints
    //But that's probably too early in 2019 :)
    
	template<typename T>
	static int f(T x)
	{
		std::cout << "x is a " << typeid(x).name() << "\n";
		return default_value;
	}

	template<typename T>
	static int f(std::vector<T> x)
	{
		std::cout << "x is a vector" << '\n';
		int result = 0;
		for (const T& element : x)
			result = (result + f(element)) % 869;
		return result;
	}

	template<typename T, typename U>
	static int f(std::pair<T, U> const& x)
	{
		std::cout << "x is a pair" << '\n';
		return (f(x.first) * (3 + f(x.second))) % 569;
	}

	template<typename T, typename... Args>
	static int f(T x, Args... args)
	{
		return (f(x) + f(args...)) % 869;
	}
};

template<>
int task2::f(int x);
template<>
int task2::f(float x);
template<>
int task2::f(char const* x);

template<typename T>
int task2::factorial_mod(T n, int mod) {
	long long int result = 1;
	for (int i = 1; i <= n; i++)
	{
		result *= i;
		result %= mod;
		if (result == 0) //fun fact - since 169 = 13^2, any n >= 13*2 will result in 0.
			break;
	}
	return result;
}

template<typename T>
int task2::f_positive_int(T x)
{
	std::cout << "x is a positive integer.\n";
	const int modulo = 169;
	return (factorial_mod(x, modulo) + x - 1) % modulo;
}

template<typename T>
int task2::f_negative_int(T x)
{
	std::cout << "x is a negative integer.\n";
	const int modulo = 269;
	long long int result = x;
	for (int i = 0; i < 4; i++)
		result = (result * x) % modulo;
	return (int)result;
}