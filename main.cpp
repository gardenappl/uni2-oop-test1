﻿#include <iostream>
//#include "democratic_world.h"
#include "task2.h"
#include "task3.h"

int main()
{
	std::cout << "\n\nTASK 2\n\n";
	std::cout << task2::f(172) << '\n';
	std::cout << task2::f(50.1f) << '\n';
	std::cout << task2::f(50.1f, 172) << '\n';
	std::cout << task2::f(-1) << '\n';
	std::cout << task2::f("hey Hey HEY ПРИВЕТ") << '\n';
	std::cout << task2::f(std::make_pair(69, 70.1f)) << '\n';
	std::cout << task2::f(69, "I have a string", "I have a BIGGER STRING", 4.20f) << '\n';

	std::vector<int> vec;
	vec.push_back(1);
	vec.push_back(2);
	vec.push_back(3);
	std::cout << task2::f<int>(vec) << '\n';

	int* a = new int[3];
	std::cout << task2::f<int*>(a) << '\n';
	delete[] a;

	std::cout << "\n\nTASK 3\n\n";

	{
		std::shared_ptr<task3::base1> a1 = std::make_shared<task3::alpha>();
		a1->child1 = std::make_shared<task3::alpha>();
		a1->child2_1 = std::make_shared<task3::delta>();
		a1->child2_1->child2_2 = std::make_shared<task3::gamma>();
		a1->child2_2 = a1->child2_1->child2_2;

		std::shared_ptr<task3::base1> b1 = std::make_shared<task3::beta>();
		b1->child1 = std::shared_ptr<task3::base1>(a1);

		std::shared_ptr<task3::base2> c2 = std::make_shared<task3::gamma>();
		c2->child2_2 = a1->child2_2;

		//std::cout << "Curiously, shared pointers get deleted after b1's destructors are called.\n";
	}

	std::cout << " s = " << task3::_s << std::endl;

	task3::_s = 0;
	task3::_alpha_n = 1;
	task3::_beta_n = 1;
	task3::_gamma_n = 1;
	task3::_delta_n = 1;

	
	{
		std::cout << " === BEGIN SIMULATION === " << std::endl;
		std::vector<std::shared_ptr<task3::base>> objects;

		objects.push_back(std::make_shared<task3::alpha>()); //1 1
		objects[0]->child1 = std::make_shared<task3::alpha>(); //2 1
		objects[0]->child2_1 = std::make_shared<task3::delta>(); //1 1
		objects[0]->child2_1->child2_2 = std::make_shared<task3::gamma>(); //1 2
		objects[0]->child2_2 = objects[0]->child2_1->child2_2;

		objects.push_back(std::make_unique<task3::beta>()); //1 1
		objects[1]->child1 = std::static_pointer_cast<task3::base1>(objects[0]);

		objects.push_back(std::make_unique<task3::gamma>()); //2 1
		objects[2]->child2_2 = objects[0]->child2_2;

		int simulated_result = task3::simulate_deletion(objects);
		std::cout << " s = " << simulated_result << std::endl;
		std::cout << " === END SIMULATION === " << std::endl;
	}

}
