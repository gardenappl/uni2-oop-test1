#pragma once

#include <vector>
#include <memory>
#include <unordered_map>
#include <limits>
#include <optional>
#include <random>

#include "democratic_world.fwd.h"
#include "laws.h"
#include "components.h"

namespace demo
{

	class party
	{
	private:
		std::vector<std::unique_ptr<party_law_selection_strategy>> election_strategies;
		std::unique_ptr<party_vote_strategy> approval_likelihood_strategy;


	public:
		std::vector<size_t> supported_laws;
		std::vector<size_t> opposed_laws;

		party(std::vector<std::unique_ptr<party_law_selection_strategy>>&&,
				party_vote_strategy*);

		size_t select_preferred_law(std::vector<size_t>& available_laws, world* the_world, parliament* the_parliament);
		float calculate_approval(law* a_law, parliament* the_parliament, world* the_world);
	};

	class voter
	{
	private:
		std::vector<std::unique_ptr<voter_election_strategy>> election_strategies;


	public:
		std::vector<size_t> supported_laws;
		std::vector<size_t> opposed_laws;

		voter(std::vector<std::unique_ptr<voter_election_strategy>>&&);
		party* select_preferred_law(std::vector<party*>& parties, world* the_world);
	};

	//The parliament class is split into components

	class parliament
	{
		world* the_world;

		int last_election_day = 0;

		std::optional<size_t> current_debated_law_id;
		int current_law_debate_length = 0;

	public:
		//Components
		law_selection_process_component* law_selection_process;
		election_process_component* election_process;
		law_debate_length_component* law_debate_length;
		law_vote_threshold_component* law_vote_threshold;
		int term_length;

		//Everything else
		std::vector<party*> parties;
		//I'm not going to model individual representatives here
		std::unordered_map<size_t, int> representatives_count;
		std::vector<law*> suggested_laws_list;

		parliament(world* the_world, int term_length,
				law_selection_process_component*, election_process_component*,
				law_debate_length_component*, law_vote_threshold_component*
				);

		std::unique_ptr<std::unordered_map<party*, size_t>> select_preferred_law();

		//Returns true if the parliament has achieved anything today
		bool pass_day();
		void debate_law();
	};

	struct voting_results
	{
		std::unordered_map<party, int> vote_count;
	};


	class world
	{
	public:
		std::vector<std::unique_ptr<voter>> voters;
		std::vector<std::unique_ptr<party>> parties;
		std::vector<law*> current_laws;
		//all_laws contains all the laws that can be debated, liked, hated, etc.
		std::vector<std::unique_ptr<law>> all_laws;
		std::default_random_engine rng;

		std::vector<std::unique_ptr<law_selection_process_component>> law_selection_process_components;


		parliament the_parliament;
		int current_day = 0;

		void do_election();
		bool pass_day();
	};
}